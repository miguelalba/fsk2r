## This is a resubmission

In this version I have:

* Added a link to DESCRIPTION describing the FSK-ML standard.
* Reduced the size of the files in /inst, so they are less than 5MB

## Test environments
* local OS X install, R 3.5.3
* ubuntu 14.04 (on travis-ci), R 3.5.3
* win-builder (devel and release)

## R CMD check results

0 errors | 0 warnings | 2 notes

* This is a new release.

> Possibly mis-spelled words in DESCRIPTION:
  FSK (3:33, 11:15, 12:41)
  
FSK is an acronym for Food Safety Knowledge. In the new version, I have added a link to a webpage describing it.

> Non-FOSS package license (file LICENSE)

The package is part of a project ordered by the German Food Safety Agency (Bundesinstitut fur Risikobewertung). Due to contract issues, we have to use a special license. Nonetheless, it is a standard GPL-3 license with some small additions that should not affect the 'openness' of the package.


